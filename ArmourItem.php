<?php

require_once("./Item.php");

class ArmourItem extends Item
{

    /**
     * @var String
     */
    public $typeOfArmour;

    function __construct($name, $typeOfArmour)
    {
        parent::__construct($name, false);
        $this->typeOfArmour = $typeOfArmour;
    }
}
