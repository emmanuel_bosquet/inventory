<?php

class Item
{

    /**
     * @var String 
     */
    public $name;

    /**
     * @var bool
     */
    public $stackable;

    function __construct($name, $stackable)
    {
        $this->name = $name;
        $this->stackable = $stackable;
    }
}
