<?php

require_once("./Slot.php");
require_once("./Item.php");

class GenericSlot extends Slot
{

    /**
     * @var int
     */
    public $slotNumber;

    /**
     * @var int
     */
    private $capacity;

    /**
     * @param int capacity
     */
    function __construct($slotNumber, $capacity)
    {
        parent::__construct();
        $this->slotNumber = $slotNumber;
        $this->capacity = $capacity;
        $this->items = [];
    }

    /**
     * @return bool wether the item was accepted
     */
    function addItem($item)
    {
        // refuse the item if the slot is full
        if (sizeof($this->items) == $this->capacity) {
            return false;
        }

        // accept any item if the slot is empty
        if (sizeof($this->items) == 0) {
            array_push($this->items, $item);
            return true;
        }

        // else, check if the item has the same name like the others
        // and check if the item is stackable
        if ($this->items[0]->name == $item->name && $item->stackable) {
            array_push($this->items, $item);
            return true;
        }

        return false;
    }

    /**
     * take an item from the slot
     */
    function takeItem()
    {
        // create an empty item because why not hahaha
        if (sizeof($this->items) == 0) {
            $emptyItem = new Item("empty", false);
            return $emptyItem;
        }

        return array_pop($this->items);
    }

    function render() {
        if (sizeof($this->items) == 0) {
            return;
        }
        if (sizeof($this->items) == 1 ) {
            echo "Slot " . $this->slotNumber . " : " . $this->items[0]->name . "\n";
            return;
        }
        echo "Slot " . $this->slotNumber . " : " . $this->items[0]->name . " (" . sizeof($this->items) . ")\n";
    }
}
