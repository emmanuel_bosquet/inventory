<?php

require_once("./Slot.php");
require_once("./ArmourItem.php");

class ArmourSlot extends Slot
{
    /**
     * @var String
     */
    public $typeOfArmour;

    function __construct($typeOfArmour)
    {
        $this->typeOfArmour = $typeOfArmour;
    }

    function addItem($armourItem)
    {
        if ($this->full) {
            echo "Could not add " . $armourItem->name . " to " . $this->typeOfArmour . " slot, it is used already.\n";
            return false;
        }
        if ($this->typeOfArmour == $armourItem->typeOfArmour) {
            $this->item = $armourItem;
            echo "Added " . $armourItem->name . " to the " . $this->typeOfArmour . " slot\n";
            $this->full = true;
            return true;
        } else {
            return false;
        }
    }

    // no need to override takeItem()

    function render()
    {
        if ($this->item != null) {

            echo $this->typeOfArmour . ": [ " . $this->item->name . " ]\n";
        } else {
            echo $this->typeOfArmour . ": [ - ]\n";
        }
    }
}
