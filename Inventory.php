<?php

require_once("./ArmourItem.php");
require_once("./ArmourSlot.php");

require_once("./GenericSlot.php");
require_once("./ArmourSlot.php");

class Inventory
{
    /**
     * @property ArmourSlot
     */
    public $headSlot;

    /**
     * @property ArmourSlot
     */
    public $torsoSlot;

    /**
     * @property ArmourSlot
     */
    public $legSlot;

    /**
     * @property ArmourSlot
     */
    public $shoeSlot;

    /**
     * @property int
     */
    public $numberOfGenericSlots;

    /**
     * @property GenericSlot[]
     */
    public $genericSlots;

    function __construct($numberOfGenericSlots)
    {
        $this->headSlot = new ArmourSlot("head");
        $this->torsoSlot = new ArmourSlot("torso");
        $this->legSlot = new ArmourSlot("legs");
        $this->shoeSlot = new ArmourSlot("shoes");

        $this->numberOfGenericSlots = $numberOfGenericSlots;

        $genericSlots = [];
        for ($i = 1; $i <= $numberOfGenericSlots; $i++) {
            array_push($genericSlots, new GenericSlot($i, 64));
        }
        $this->genericSlots = $genericSlots;
    }

    /**
     * @param GenericItem
     */
    function addItemToSlot($item, $slotNumber)
    {
        if ($slotNumber > $this->numberOfGenericSlots) {
            echo "This slot doesn't even exist!\n";
        }
        if ($this->genericSlots[$slotNumber - 1]->addItem($item)) {
            echo "Successfully added " . $item->name . " to generic slot " . $slotNumber . "\n";
        } else {
            echo "Could not add " . $item->name . " to generic slot " . $slotNumber . "\n";
        }
    }

    /**
     * Print everything to the console
     */
    function render()
    {
        echo "=========== INVENTORY ===========\n";
        echo "\n";
        echo "------------- ARMOUR ------------\n";
        $this->headSlot->render();
        $this->torsoSlot->render();
        $this->legSlot->render();
        $this->shoeSlot->render();
        echo "\n";
        echo "-------------- ITEMS ------------\n";
        foreach ($this->genericSlots as $genericSlot) {
            $genericSlot->render();
        }
    }
}
