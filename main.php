<?php 

require_once("./Inventory.php");
require_once("./ArmourItem.php");
require_once("./Item.php");

// make an inventory
$inventory = new Inventory(20);

// make items
$sword = new Item("Iron sword", false);
$arrow = new Item("Arrow", true);
$helmet = new ArmourItem("Helmet", "head");
$plateArmour = new ArmourItem("Plate armour", "torso");
$chainMail = new ArmourItem("Chain mail", "torso");

$inventory->addItemToSlot($sword, 1);

$inventory->addItemToSlot($arrow, 2);
$inventory->addItemToSlot($arrow, 2);

$inventory->headSlot->addItem($helmet);
$inventory->torsoSlot->addItem($chainMail);
$inventory->torsoSlot->addItem($plateArmour);

$inventory->render();


