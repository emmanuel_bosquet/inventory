<?php

require_once("./Item.php");

class Slot
{
    /**
     * @property Item
     */
    var $item;

    /**
     * @property bool
     */
    var $full;

    function __construct()
    {
        $this->full = false;
    }

    /**
     * Returns true if the slot can take the item
     * @param Item
     * @return bool
     */
    function addItem($item)
    {
        return true;
    }

    /**
     * @return Item
     */
    function takeItem()
    {
        $this->full = false;
        return $this->item;
    }
}
